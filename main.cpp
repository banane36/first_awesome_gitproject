// main.cpp
// 13-12-2021

#include <iostream>

int main() {
  int ival1 = 123, ival2 = 0;
  int* iptr = nullptr;
  // iptr die Adresse von ival1 übergeben
  iptr = &ival1;
  // ival2 erhält indirekt den Wert ival1
  ival2 = *iptr;
  std::cout << "ival1 : " << ival1 << std::endl;  // 123
  std::cout << "ival2 : " << ival2 << std::endl;  // 123
  return 0;
}
