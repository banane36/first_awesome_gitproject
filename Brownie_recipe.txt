10 tablespoons (145 grams) unsalted butter

1.25 cups (250 grams) granulated sugar

3/4 cup plus 2 tablespoons (80 grams) unsweetened cocoa powder, natural or Dutch-process

1/4 rounded teaspoon kosher salt, use slightly less if using fine sea or table salt

1 teaspoon vanilla extract

2 large cold eggs

1/2 cup (65 grams) all-purpose flour

2/3 cup (75 grams) chopped walnuts or pecans, optional
