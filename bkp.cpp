bkp/main.cpp

#include "monatseinnahme.h"
#include "lliste.h"


void input_func( Liste<Monatseinnahmen>& datenList );
template <class T> void input( Liste<T>& data );

int main( void ) {
   // Instanziierung
   Liste<Monatseinnahmen> elemente;
   input(elemente); 
   elemente.alles_anzeigen();
   return 0;
}  

// Funktion zum Einlesen der Klassen-Daten fŸr Daten
void input_func( Liste<Monatseinnahmen>& datenList ) {
   Monatseinnahmen* daten;
   int iwert;
   string monat;
   
   for(;;) {
      cout << "Monat eingeben (0=Ende)     : ";
      if(!(cin >> monat) || monat == "0")
         break; // Ende     
      cout << "Einnahmen eingeben (0=Ende) : ";
      if( (!(cin >> iwert)) || iwert == 0 )
         break; // Ende      
      daten = new Monatseinnahmen(iwert, monat);
      datenList.einfuegen(daten);
   }
} 

// Funktions-Template 
template <class T>
void input( Liste<T>& data ) {
     input_func( data );
}
