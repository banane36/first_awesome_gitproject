// lliste.h


#include <iostream>
#ifndef _LLISTE_H_
#define _LLISTE_H_

// abstrakte Basisklassen-Template Knoten<T>
// Alle abgeleiteten Klassen m�ssen "einfuegen" 
// und "anzeigen" redefinieren
template <class T>
class Knoten {
   public:
   virtual ~Knoten() {}
   virtual Knoten* einfuegen( T* d ) = 0;
   virtual void anzeigen() = 0;
};

// ------ Abgeleitete Klasse AllgemeinerKnoten ------------
// Diese Klasse ist f�r die eigentliche Verwaltung der Daten
// verantwortlich 
template <class T>
class AllgemeinerKnoten : public Knoten<T> {
   private:
   T* daten;
   Knoten<T>* next;
   public:
   AllgemeinerKnoten(T* d, Knoten<T>* n):daten(d), next(n) {}
   ~AllgemeinerKnoten() {
      delete next;
      delete daten;
   }
   Knoten<T>* einfuegen( T* d );
   void anzeigen();
};


template <class T>
Knoten<T>* AllgemeinerKnoten<T>::einfuegen( T* d ) {
   // Wir sortieren aufw�rts - kleiner Wert vor gro�en Wert
   int ret = daten->vergleichen( *d );
   switch( ret ) {
      case 0:
      case 1: {
         // Neue Daten vor dem aktuellen einordnen
         AllgemeinerKnoten<T>* dKnoten =
            new AllgemeinerKnoten<T>( d, this );
         return dKnoten;
      }   
      case -1:
         // gr��er als das akt. Elmement - 
         // weiter zum n�chsten Knoten
         next = next->einfuegen( d );
         return this;
   }
   return this;
}

template <class T>
void AllgemeinerKnoten<T>::anzeigen() {
   daten->anzeigen();
   next->anzeigen();
} 

// ------ Abgeleitete Klasse EndKnoten -------------------
// Der EndKnoten dient im Grund nur als Endpunkt der Liste
template <class T>
class EndKnoten : public Knoten<T> {
   public:
   ~EndKnoten() {}
   Knoten<T>* einfuegen( T* d );
   void anzeigen() { };
};

// Daten werden immer vor dem Ende einf�gen
template <class T>
Knoten<T>* EndKnoten<T>::einfuegen( T* d ) {
   AllgemeinerKnoten<T>* daten =
      new AllgemeinerKnoten<T>(d, this);
   return daten;
}

// ------ Abgeleitete Klasse AnfangsKnoten ---------------
// Knoten der "nur" immer auf das erste Element der
// Liste zeigt
template <class T>
class AnfangsKnoten : public Knoten<T> {
   private:
   // ... zeigt immer auf das erste Element
   Knoten<T>* next;
   public:
   AnfangsKnoten<T>() {
      // ... gleich auch einen Endknoten erzeugen                   
      next = new EndKnoten<T>;
   }
   ~AnfangsKnoten() {}
   Knoten<T>* einfuegen( T* d );
   void anzeigen();  
};

template <class T>
Knoten<T>* AnfangsKnoten<T>::einfuegen( T* d ) {
   // Am Anfang kommen keine Daten rein, daher an den 
   // n�chsten Knoten weiter reichen        
   next = next->einfuegen(d);
   return this;
}

template <class T>
void AnfangsKnoten<T>::anzeigen() {
   next->anzeigen();
}

// ------ Unabh�ngige Klasse Liste -------------------
template <class T>
class Liste {
   private:
   AnfangsKnoten<T> *anfang;
   public:
   // Bei Anlegen gleich ein Objekt "Anfangsknoten" erzeugen 
   // der Konstruktor von "AnfangsKnoten" erzeugt wiederum
   // ein Objekt "EndKnoten", auf dass dieser gleich zeigt.
   Liste() { anfang = new AnfangsKnoten<T>; }
   ~Liste() { delete anfang; }
   void einfuegen( T* d ) {
      anfang->einfuegen(d);
   }
   void alles_anzeigen() {
      anfang->anzeigen();
   }
};
#endif
