// monatseinnahme.h

#include <iostream>
#include <string>
using namespace std;


#ifndef _MONATSEINNAHME_H_
#define _MONATSEINNAHME_H_

class Monatseinnahmen {
   private:
     int einnahme;
     string monat;
   public:
     Monatseinnahmen( int val=0, string m="" ) 
       :einnahme(val), monat(m) { }
     int vergleichen( const Monatseinnahmen& d) {
       if( einnahme > d.einnahme ) { return 1; };
       if( einnahme < d.einnahme ) { return -1;  };
       return 0;   
     }
     void anzeigen() const {
       cout << monat << ": " << einnahme << endl;
     }
};

#endif
